import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../models/product.model.dart';
import '../tiles/product.tile.dart';

class ProductScreen extends StatelessWidget {

  final DocumentSnapshot snapshot;

  ProductScreen(this.snapshot);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text(snapshot.data["title"]),
          centerTitle: true,
          bottom: TabBar(
            indicatorColor: Colors.white,
            tabs: <Widget>[
              Tab(icon: Icon(Icons.grid_on),),
              Tab(icon: Icon(Icons.list),),
            ],
          ),
        ),
        body: FutureBuilder(
          future: Firestore.instance.collection("products").document(snapshot.documentID).collection("itens").getDocuments(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(child: CircularProgressIndicator(),);
            } else {
              return TabBarView(
                physics: NeverScrollableScrollPhysics(),
                children: <Widget>[
                  _getGridView(snapshot),
                  _getListView(snapshot),
                ],
              );
            }
          },
        ),
      ),
    );
  }

  GridView _getGridView(AsyncSnapshot<QuerySnapshot> snapshot) {
    return GridView.builder(
      padding: EdgeInsets.all(4.0),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        childAspectRatio: 0.65,
        crossAxisCount: 2,
        mainAxisSpacing: 4.0,
        crossAxisSpacing: 4.0
      ),
      itemCount: snapshot.data.documents.length,
      itemBuilder: (context, index) {
        return ProductTile(ProductTile.TYPE_GRID, ProductModel.fromDocument(snapshot.data.documents[index]));
      },
    );
  }

  ListView _getListView(AsyncSnapshot<QuerySnapshot> snapshot) {
    return ListView.builder(
      padding: EdgeInsets.all(4.0),
      itemCount: snapshot.data.documents.length,
      itemBuilder: (context, index) {
        return ProductTile(ProductTile.TYPE_LIST, ProductModel.fromDocument(snapshot.data.documents[index]));
      },
    );
  }

}