import 'package:cloud_firestore/cloud_firestore.dart';

class ProductModel {
  
  String category;
  String id;

  String title;
  String description;

  double price;

  List images;
  List size;

  ProductModel.fromDocument(DocumentSnapshot snapshot) {
    this.id = snapshot.documentID;
    this.title = snapshot.data["title"];
    this.description = snapshot.data["description"];
    this.price = snapshot.data["price"];
    this.images = snapshot.data["images"];
    this.size = snapshot.data["size"];
  }

}