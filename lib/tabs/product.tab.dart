import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../tiles/category.tile.dart';

class ProductsTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Firestore.instance.collection("products").getDocuments(),
      builder: (context, snapshot) {
        if(!snapshot.hasData) {
          return Center(child: CircularProgressIndicator(),);
        } else {
          List<Widget> products = List();
          snapshot.data.documents.forEach((doc) => products.add(CategoryTile(doc)));
          var dividerTiles = ListTile.divideTiles(
            tiles: products,
            color: Colors.grey[400],
          ).toList();
          return ListView(
            children: dividerTiles,
          );
        }
      },
    );
  }
}