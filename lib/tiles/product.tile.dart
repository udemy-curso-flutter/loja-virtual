import 'package:flutter/material.dart';

import '../models/product.model.dart';

class ProductTile extends StatelessWidget { 

  static const TYPE_GRID = "grid";
  static const TYPE_LIST = "list";

  final ProductModel productModel;
  final String type;

  ProductTile(this.type, this.productModel);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Card(
        child: _renderGridListView(context),
      ),
    );
  }

  Widget _renderGridListView(BuildContext context) {
    Widget gridOrList;
    if (this.type == TYPE_GRID) {
      gridOrList = _getGridView(context);
    } else {
      gridOrList = _getListView(context);
    }
    return gridOrList;
  }

  Column _getGridView(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        AspectRatio(
          aspectRatio: 0.8,
          child: Image.network(
            this.productModel.images[0],
            fit: BoxFit.cover,
          ),
        ),
        Expanded(
          child: Container(
            padding: EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                Text(
                  this.productModel.title,
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Text(
                  "R\$ ${this.productModel.price.toStringAsFixed(2)}",
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 17.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Row _getListView(BuildContext context) {
    return Row(
      children: <Widget>[
        Flexible(
          flex: 1,
          child: Image.network(
            this.productModel.images[0],
            fit: BoxFit.cover,
            height: 250.0,
          ),
        ),
        Flexible(
          flex: 1,
          child: Container(
            padding: EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  this.productModel.title,
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Text(
                  "R\$ ${this.productModel.price.toStringAsFixed(2)}",
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 17.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

}